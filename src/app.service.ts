import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {

  response = {
    message: 'Hello from the NestJS MVC!'
  }

  getHello(): any {
    return this.response;
  }
}
