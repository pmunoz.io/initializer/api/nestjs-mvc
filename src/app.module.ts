import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { env } from './config/env';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { AuthController } from './auth/controller/auth.controller';

@Module({
  imports: [
    MongooseModule.forRoot(env.dbUrl + env.dbName, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }),
    AuthModule,
    UserModule
  ],
  controllers: [AppController, AuthController],
  providers: [AppService],
})
export class AppModule { }
