import { ResponseObject } from '../shared/model/response-object.interfaces';

export class GenericResponses {

  static STATUS_CODES = {
    OK: 200,
    BAD_REQUEST: 400,
    FORBIDDEN: 403,
    INTERNAL_SERVER_ERROR: 500,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404
  };

  static ERROR_TYPES = {
    BAD_REQUEST: 'Bad Request',
    FORBIDDEN: 'Forbidden',
    INTERNAL_SERVER_ERROR: 'Internal server error',
    UNAUTHORIZED: 'Unauthorized',
    NOT_FOUND: 'Not Found',
    OK: ''
  };

  static SUCCESS_MESSAGES = {
    RESPONSE_CREATED_MESSAGE: 'Element was successfully created',
    RESPONSE_QUERY_MESSAGE: 'Query executed successfully',
    RESPONSE_DELETED_MESSAGE: 'Element was successfully deleted',
    RESPONSE_UPDATED_MESSAGE: 'Element was successfully updated'
  };

  static ERROR_MESSAGES = {
    FORBIDDEN_MESSAGE: 'You do not have access to what you have requested',
    INTERNAL_SERVER_ERROR_MESSAGE: 'Something went wrong in the server! Please, try later',
    NOT_FOUND_MESSAGE: 'Element was not found',
    UNAUTHORIZED_MESSAGE: 'You do not have valid access credentials'
  };

  static FORM_VALIDATION = {
    REQUIRED: 'Field must be filled out',
    EMAIL_FORMAT: 'Please fill a valid email address'
  };

  public static GENERIC_BAD_REQUEST(message: string): ResponseObject<any> {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.INTERNAL_SERVER_ERROR,
      error: this.ERROR_TYPES.INTERNAL_SERVER_ERROR,
      message,
      data: null
    }
    return response;
  }

  public static GENERIC_FORBIDDEN(): ResponseObject<any> {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.FORBIDDEN,
      error: this.ERROR_TYPES.FORBIDDEN,
      message: this.ERROR_MESSAGES.FORBIDDEN_MESSAGE,
      data: null
    }
    return response;
  }

  public static GENERIC_NOT_FOUND(): ResponseObject<any> {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.NOT_FOUND,
      error: this.ERROR_TYPES.NOT_FOUND,
      message: this.ERROR_MESSAGES.NOT_FOUND_MESSAGE,
      data: null
    }
    return response;
  }

  public static GENERIC_UNAUTHORIZED(): ResponseObject<any> {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.UNAUTHORIZED,
      error: this.ERROR_TYPES.UNAUTHORIZED,
      message: this.ERROR_MESSAGES.UNAUTHORIZED_MESSAGE,
      data: null
    };
    return response;
  }

  public static GENERIC_INTERNAL_SERVER_ERROR(): ResponseObject<any> {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.INTERNAL_SERVER_ERROR,
      error: this.ERROR_TYPES.INTERNAL_SERVER_ERROR,
      message: this.ERROR_MESSAGES.INTERNAL_SERVER_ERROR_MESSAGE,
      data: null
    };
    return response;
  }

  public static GENERIC_ELEMENT_CREATED(data: any) {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.OK,
      error: this.ERROR_TYPES.OK,
      message: this.SUCCESS_MESSAGES.RESPONSE_CREATED_MESSAGE,
      data: data
    };
    return response;
  }

  public static GENERIC_ELEMENT_FOUND(data: any) {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.OK,
      error: this.ERROR_TYPES.OK,
      message: this.SUCCESS_MESSAGES.RESPONSE_QUERY_MESSAGE,
      data: data
    };
    return response;
  }

  public static GENERIC_ELEMENT_DELETED(data: any) {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.OK,
      error: this.ERROR_TYPES.OK,
      message: this.SUCCESS_MESSAGES.RESPONSE_DELETED_MESSAGE,
      data: data
    };
    return response;
  }

  public static GENERIC_ELEMENT_UPDATED(data: any) {
    const response: ResponseObject<any> = {
      statusCode: this.STATUS_CODES.OK,
      error: this.ERROR_TYPES.OK,
      message: this.SUCCESS_MESSAGES.RESPONSE_UPDATED_MESSAGE,
      data: data
    };
    return response;
  }
}