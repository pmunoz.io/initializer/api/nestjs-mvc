import { AuthDto } from './../dto/auth.dto';
import { RESPONSE_USER, MOCK_USERS } from '../../../user/model/mock/user.mock';
import { USER_REMOVE_KEYS } from '../../../user/constant/user.constant';
import { GenericResponses } from '../../../constant/generic-response.constant';
import * as _ from 'underscore';

interface Auth extends AuthDto {
  email: string;
  password: string;
};

const NAME_VALIDATION_EXCEPTION = 'User validation failed: name: Please fill a valid name';

export const MOCK_TOKEN = {
  accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmluY2lwYWwiOnsiZW1haWwiOiJleGFtcGxlQGV4YW1wbGUuY29tIiwicm9sZSI6ImJhc2ljVXNlciIsImlkIjoiZXhhbXBsZUlkIn0sImlhdCI6MTU4NDM2MzUxNiwiZXhwIjoxNTg0MzYzODE2fQ.oYCAT1d8AZUJ2DNthiISUVcE21mGcJgBM2Qzuiaue38'
};

export const MOCK_AUTH: Auth = {
  email: 'email1@mail.com',
  password: 'pass1'
};

export const SERVICE_MOCK = {
  login: jest.fn().mockResolvedValueOnce(MOCK_TOKEN),
  register: jest.fn().mockResolvedValueOnce(RESPONSE_USER),
};

export const FOUND_ONE_RESPONSE = GenericResponses.GENERIC_ELEMENT_FOUND(MOCK_USERS[0]);
export const UNAUTHORIZE_RESPONSE = GenericResponses.GENERIC_UNAUTHORIZED();
export const CREATE_RESPONSE = GenericResponses.GENERIC_ELEMENT_CREATED(_.omit(MOCK_USERS[0], USER_REMOVE_KEYS));
export const BAD_REQUEST_RESPONSE = GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
export const INVALID_REQUEST_RESPONSE = GenericResponses.GENERIC_BAD_REQUEST(NAME_VALIDATION_EXCEPTION);

export class MockFactory {

  save = jest.fn().mockResolvedValue(_.omit(MOCK_USERS[0], USER_REMOVE_KEYS));
  static findOne = jest.fn().mockResolvedValue(MOCK_USERS[0]);
};

export class MockFactoryFail {
  static save() {
    return { exec: jest.fn().mockRejectedValue(null) };
  };

  findOne = jest.fn().mockResolvedValue(null);

  static sign() {
    return null;
  }
}

export class MockFactoryValidationFail {
  save = jest.fn().mockRejectedValue({message: NAME_VALIDATION_EXCEPTION});
}

export class MockJWTService {

  static sign = jest.fn().mockReturnValue(MOCK_TOKEN.accessToken);

}