import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from './service/auth.service';
import { env } from './../config/env';
import { JwtStrategy } from './strategy/jwt.strategy';
import { userSchema } from '../user/model/schema/user.schema';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: env.secret,
      signOptions: { expiresIn: env.timeSession },
    }),
    MongooseModule.forFeature([{ name: 'User', schema: userSchema }])
  ],
  providers: [
    JwtStrategy,
    AuthService
  ],
  exports: [AuthService]
})
export class AuthModule { }
