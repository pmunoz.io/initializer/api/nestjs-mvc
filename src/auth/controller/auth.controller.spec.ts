import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from '../service/auth.service';
import { AuthController } from './auth.controller';
import { MOCK_USERS, RESPONSE_USER } from '../../user/model/mock/user.mock';
import { SERVICE_MOCK, MOCK_AUTH, MOCK_TOKEN } from '../model/mock/auth.mock';

describe("AuthService", () => {
  let authController: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [{
        provide: AuthService,
        useValue: SERVICE_MOCK
      }]
    }).compile();
    authController = module.get<AuthController>(AuthController);
  });

  test('should be defined', () => {
    expect(authController).toBeDefined();
  });

  test('should authenticate an user', async () => {
    const response = await authController.login(MOCK_AUTH);
    expect(response).toEqual(MOCK_TOKEN);
    expect(SERVICE_MOCK.login).toHaveBeenCalled();
  });

  test('should register an user', async () => {
    const response = await authController.save(MOCK_USERS[0]);
    expect(response).toEqual(RESPONSE_USER);
    expect(SERVICE_MOCK.register).toHaveBeenCalled();
  });

});