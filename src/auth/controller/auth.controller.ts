import {
  Controller,
  Post,
  Body
} from '@nestjs/common';

import { AuthDto } from './../model/dto/auth.dto';
import { AuthService } from '../service/auth.service';
import { UserDto } from '../../user/model/dto/user.dto';
import { User } from '../../user/model/interface/user.interface';
import { ResponseObject } from '../../shared/model/response-object.interfaces';

@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService) { }

  @Post('login')
  async login(@Body() credentials: AuthDto): Promise<any> {
    const { email, password } = credentials;
    return await this.authService.login(email, password);
  }

  @Post('register')
  async save(@Body() userDto: UserDto): Promise<ResponseObject<User>> {
    return await this.authService.register(userDto);
  }
}