import { Test, TestingModule } from '@nestjs/testing';

import { AuthService } from './auth.service';
import {
  MockFactory,
  FOUND_ONE_RESPONSE,
} from '../model/mock/auth.mock';
import { getModelToken } from '@nestjs/mongoose';
import { MOCK_USERS } from '../../user/model/mock/user.mock';
import {
  CREATE_RESPONSE,
  MockJWTService,
  MockFactoryFail,
  BAD_REQUEST_RESPONSE,
  UNAUTHORIZE_RESPONSE
} from '../model/mock/auth.mock';
import { JwtService, JwtModule } from '@nestjs/jwt';

import { env } from '../../config/env';
import { MOCK_TOKEN, MOCK_AUTH } from '../model/mock/auth.mock';

describe('AuthService', () => {
  describe('Expected path', () => {
    let service: AuthService;
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [
          JwtModule.register({
            secret: env.secret,
            signOptions: { expiresIn: env.timeSession },
          })
        ],
        providers: [
          AuthService,
          {
            provide: JwtService,
            useValue: MockJWTService
          },
          {
            provide: getModelToken('User'),
            useValue: MockFactory
          }
        ]
      }).compile();
  
      service = module.get<AuthService>(AuthService);
    });

    test('should be defined', () => {
      expect(service).toBeDefined();
    });
  
    test('should not authenticate if the user does not exists', async () => {
      const email = 'notFound'
      const response = await service.login(email, MOCK_AUTH.password);
      expect(response).toEqual(UNAUTHORIZE_RESPONSE);
    });
  
    test('should register an user if the form was right', async () => {
      const response = await service.register(MOCK_USERS[0]);
      expect(response).toEqual(CREATE_RESPONSE);
    });
  
    test('should get one user if the email exists', async () => {
      const email = MOCK_AUTH.email;
      const response = await service.findByEmail(email);
      expect(response).toEqual(FOUND_ONE_RESPONSE.data);
    });
  
    test('should authenticate if the user exists', async () => {
      const response = await service.login(MOCK_AUTH.email, MOCK_AUTH.password);
      expect(response).toEqual(MOCK_TOKEN);
    });
  });

  describe('Exception control', () => {
    let service: AuthService;
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [
          JwtModule.register({
            secret: env.secret,
            signOptions: { expiresIn: env.timeSession },
          })
        ],
        providers: [
          AuthService,
          {
            provide: getModelToken('User'),
            useValue: MockFactoryFail
          }
        ]
      }).compile();
      service = module.get<AuthService>(AuthService);
    });

    test('should get an exception trying to register an user', async () => {
      const response = await service.register(null);
      expect(response).toEqual(BAD_REQUEST_RESPONSE);
    });

    test('should get an exception trying to finding an user by email', async () => {
      const email = null;
      const response = await service.findByEmail(email);
      expect(response).toEqual(BAD_REQUEST_RESPONSE);
    });
    
    test('should get an exception trying to get an token', async () => {
      const response = await service.login(null, MOCK_USERS[0].password);
      expect(response).toEqual(BAD_REQUEST_RESPONSE);
    });
  });

// Not plaing well with the other tests
  /* describe('Validation control', () => {
    let service: AuthService;
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [
          JwtModule.register({
            secret: env.secret,
            signOptions: { expiresIn: env.timeSession },
          })
        ],
        providers: [
          AuthService,
          {
            provide: getModelToken('User'),
            useValue: MockFactoryValidationFail
          }
        ]
      }).compile();
      service = module.get<AuthService>(AuthService);
    });

    test('should get a validation exception trying to register an invalid user', async () => {
      const response = await service.register(MOCK_USERS[0]);
      expect(response).toEqual(INVALID_REQUEST_RESPONSE);
    });
  }); */
});
