import { Injectable } from '@nestjs/common';
import { JwtPayload } from '../model/interfaces/jwt-payload.interface';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';

import { Model } from 'mongoose';
import { User } from '../../user/model/interface/user.interface';
import { GenericResponses } from '../../constant/generic-response.constant';
import { UserDto } from '../../user/model/dto/user.dto';
import { ResponseObject } from '../../shared/model/response-object.interfaces';
import { USER_REMOVE_KEYS } from '../../user/constant/user.constant';

import * as _ from 'underscore';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {

  constructor(
    @InjectModel('User') private readonly userRepositoryFactory: Model<User>,
    private readonly jwtService: JwtService
  ) { }

  async login(email: string, password: string): Promise<any> {
    try {
      const user = await this.findByEmail(email);

      if (user && bcrypt.compareSync(password, user.password)) {
        const payload: JwtPayload = { email: user.email, role: 'basicUser', id: user._id }
        const response = {
          accessToken: this.jwtService.sign({ principal: payload })
        };
        return response;
      } else {
        return GenericResponses.GENERIC_UNAUTHORIZED();
      }
    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }

  async register(userDto: UserDto): Promise<ResponseObject<User>> {
    try {
      userDto.password = bcrypt.hashSync(userDto.password, userDto.password.length);
      userDto.active = true;
      const userRepository = new this.userRepositoryFactory(userDto);
      const savedUser: User = await userRepository.save();
      return GenericResponses.GENERIC_ELEMENT_CREATED(_.omit(savedUser.toObject(), USER_REMOVE_KEYS));
    } catch (error) {
      
      if (null !== error.message && error.message.includes('validation')) {
        return GenericResponses.GENERIC_BAD_REQUEST(error.message);
      } else {
        return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
      }
    }
  }

  async findByEmail(email: string): Promise<any> {
    try {
      return await this.userRepositoryFactory.findOne({ email: email });
    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }
}
