export const env = {
  port: process.env.NMVC_BK_PORT || 3000,
  dbUrl: process.env.NMVC_DB_URL || 'mongodb://localhost:27017/',
  dbName: process.env.NMVC_DB_NAME || 'local-db',
  secret: process.env.NMVC_JWT_SECRET || 'password',
  timeSession: process.env.NMVC_JWT_DURATION || '300s'
};