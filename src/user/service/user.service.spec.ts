import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { getModelToken } from '@nestjs/mongoose';
import { USER_REMOVE_KEYS } from '../constant/user.constant';
import { MockFactoryFail } from '../model/mock/user.mock';
import {
  MockFactory,
  FOUND_ONE_RESPONSE,
  FOUND_ALL_RESPONSE,
  MOCK_USERS,
  NOT_FOUND_RESPONSE,
  UPDATE_RESPONSE,
  DELETE_RESPONSE,
  BAD_REQUEST_RESPONSE
} from '../model/mock/user.mock';
import * as _ from 'underscore';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken('User'),
          useValue: MockFactory
        }
      ]
    }).compile();
    service = module.get<UserService>(UserService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
  });

  test('should return all users', async () => {
    const response = await service.findAll();
    expect(response).toEqual(FOUND_ALL_RESPONSE);
  });

  test('should return a user by id if the request is OK and the id exists', async () => {
    const id = MOCK_USERS[0]._id;
    const response = await service.findOne(id);
    expect(response).toEqual(FOUND_ONE_RESPONSE);
  });

  test('should return a response error when the user was not found', async () => {
    const id = 'notFound';
    const response = await service.findOne(id);
    expect(response).toEqual(NOT_FOUND_RESPONSE);
  });

  test('should return the updated user if the request is good', async () => {
    const id = MOCK_USERS[0]._id;
    const response = await service.update(id, MOCK_USERS[0]);
    expect(response).toEqual(UPDATE_RESPONSE);
  });

  test('should return the deleted user if the request is OK and the id exists', async () => {
    const id = MOCK_USERS[0]._id;
    const response = await service.delete(id);
    expect(response).toEqual(DELETE_RESPONSE);
  });

  test('should return a response error when the user was not found', async () => {
    const id = 'null';
    const response = await service.delete(id);
    expect(response).toEqual(NOT_FOUND_RESPONSE);
  });

});

describe('UserService exception control', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken('User'),
          useValue: MockFactoryFail
        }
      ]
    }).compile();
    service = module.get<UserService>(UserService);
  });

  test('should return an response error', async () => {
    const response = await service.findAll();

    expect(response).toEqual(BAD_REQUEST_RESPONSE);
  });

  test('should return an response error if the request is wrong', async () => {
    const id = null;
    const response = await service.findOne(id);

    expect(response).toEqual(BAD_REQUEST_RESPONSE);
  });

  test('should return an error if the request is wrong', async () => {
    const id = null;
    const userToUpdate = _.omit(MOCK_USERS[0], USER_REMOVE_KEYS);;
    const response = await service.update(id, userToUpdate);

    expect(response).toEqual(BAD_REQUEST_RESPONSE);
  });

  test('should return a response error when the user was not found', async () => {
    const id = null;
    const response = await service.delete(id);
    expect(response).toEqual(BAD_REQUEST_RESPONSE);
  });

});