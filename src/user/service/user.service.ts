import * as bcrypt from 'bcrypt';
import * as _ from 'underscore';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { UserDto } from '../model/dto/user.dto';
import { Model } from 'mongoose';
import { User } from '../model/interface/user.interface';
import { GenericResponses } from '../../constant/generic-response.constant';
import { ResponseObject } from '../../shared/model/response-object.interfaces';
import { USER_REMOVE_KEYS } from '../constant/user.constant';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userRepositoryFactory: Model<User>) { }

  async findAll(): Promise<ResponseObject<User[]>> {
    try {
      const userRepository = this.userRepositoryFactory.find();
      const users: User[] = await userRepository.exec();

      const cleanUsers = _.map(users, (user: User) => {
        return _.omit(user.toObject(), USER_REMOVE_KEYS);
      });

      return GenericResponses.GENERIC_ELEMENT_FOUND(cleanUsers);

    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }

  async findOne(userId: string): Promise<ResponseObject<User>> {
    try {
      const userRepository = this.userRepositoryFactory.findById(userId);
      const userFound: User = await userRepository.exec();

      if (userFound) {
        return GenericResponses.GENERIC_ELEMENT_FOUND(_.omit(userFound.toObject(), USER_REMOVE_KEYS));
      } else {
        return GenericResponses.GENERIC_NOT_FOUND();
      }
    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }

  async update(userId: string, userDto: UserDto): Promise<ResponseObject<User>> {
    try {
      userDto.password = bcrypt.hashSync(userDto.password, userDto.password.length);
      const userRepository = this.userRepositoryFactory.findByIdAndUpdate(userId, userDto, { new: true, runValidators: true });
      const userUpdated: User = await userRepository.exec();

      return GenericResponses.GENERIC_ELEMENT_UPDATED(_.omit(userUpdated.toObject(), USER_REMOVE_KEYS));
    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }

  async delete(userId: string): Promise<ResponseObject<User>> {
    try {
      const userRepository = this.userRepositoryFactory.findByIdAndDelete(userId);
      const userDeleted: User = await userRepository.exec();

      if (userDeleted) {
        return GenericResponses.GENERIC_ELEMENT_DELETED(_.omit(userDeleted.toObject(), USER_REMOVE_KEYS));
      } else {
        return GenericResponses.GENERIC_NOT_FOUND();
      }

    } catch (error) {
      return GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();
    }
  }
}