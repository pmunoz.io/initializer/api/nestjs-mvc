import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserController } from './controller/user.controller';
import { userSchema } from './model/schema/user.schema';
import { UserService } from './service/user.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: userSchema }])
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule { }