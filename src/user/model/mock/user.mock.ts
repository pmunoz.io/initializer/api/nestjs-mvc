import { ResponseObject } from '../../../shared/model/response-object.interfaces';
import { GenericResponses } from '../../../constant/generic-response.constant';
import { USER_REMOVE_KEYS } from '../../constant/user.constant';
import * as _ from 'underscore';

interface User {
  _id?: string;
  name: string;
  email: string;
  password: string;
  active: boolean;
  __v?: number;
}

class UserMock implements User {
  _id?: string;
  name: string;
  email: string;
  password: string;
  active: boolean;
  __v?: number;
  toObject(): any {
    return this;
  }
}

export const MOCK_USERS: UserMock[] = [
  {
    _id: 'userId1',
    name: 'name1',
    email: 'email1@mail.com',
    password: 'pass1',
    active: true,
    __v: 0,
    toObject: function () { return this }
  },
  {
    _id: 'userId2',
    name: 'name2',
    email: 'email2@mail.com',
    password: 'pass2',
    active: true,
    __v: 0,
    toObject: function () { return this }
  },
  {
    _id: 'userId3',
    name: 'name3',
    email: 'email3@mail.com',
    password: 'pass3',
    active: true,
    __v: 0,
    toObject: function () { return this }
  },
  {
    _id: 'userId4',
    name: 'name4',
    email: 'email4@mail.com',
    password: 'pass4',
    active: true,
    __v: 0,
    toObject: function () { return this }
  },
];

export const MOCK_USER_AUTH: UserMock = {
  _id: 'userId1',
  name: 'name1',
  email: 'email1@mail.com',
  password: 'pass1',
  active: true,
  __v: 0,
  toObject: function () { return this }
};

export const RESPONSE_USERS: ResponseObject<User[]> = {
  statusCode: 200,
  error: '',
  message: 'Success',
  data: MOCK_USERS
};

export const RESPONSE_USER = {
  statusCode: 200,
  error: '',
  message: 'Success',
  data: MOCK_USERS[0]
};

export const SERVICE_MOCK = {
  findByEmail: jest.fn().mockResolvedValueOnce(RESPONSE_USERS),
  findAll: jest.fn().mockResolvedValueOnce(RESPONSE_USERS),
  findOne: jest.fn().mockResolvedValueOnce(RESPONSE_USER),
  update: jest.fn().mockResolvedValueOnce(RESPONSE_USER),
  delete: jest.fn().mockResolvedValueOnce(RESPONSE_USER)
};

const CLEAN_USERS = _.map(MOCK_USERS, (user: User) => {
  return _.omit(user, USER_REMOVE_KEYS);
});

export const FOUND_ALL_RESPONSE = GenericResponses.GENERIC_ELEMENT_FOUND(CLEAN_USERS);
export const FOUND_ONE_RESPONSE = GenericResponses.GENERIC_ELEMENT_FOUND(_.omit(MOCK_USERS[0], USER_REMOVE_KEYS));
export const NOT_FOUND_RESPONSE = GenericResponses.GENERIC_NOT_FOUND();
export const UPDATE_RESPONSE = GenericResponses.GENERIC_ELEMENT_UPDATED(_.omit(MOCK_USERS[0], USER_REMOVE_KEYS));
export const DELETE_RESPONSE = GenericResponses.GENERIC_ELEMENT_DELETED(_.omit(MOCK_USERS[0], USER_REMOVE_KEYS));
export const BAD_REQUEST_RESPONSE = GenericResponses.GENERIC_INTERNAL_SERVER_ERROR();

export class MockFactory {

  static findById(id: any) {
    if (id === MOCK_USERS[0]._id) {
      return { exec: jest.fn().mockResolvedValue(MOCK_USERS[0]) };
    } else {
      return { exec: jest.fn().mockResolvedValue(null) };
    }
  };

  static find() {
    return { exec: jest.fn().mockResolvedValue(MOCK_USERS) };
  };

  static findByIdAndUpdate() {
    return { exec: jest.fn().mockResolvedValue(MOCK_USERS[0]) };
  };

  static findByIdAndDelete(id: any) {
    if (id === MOCK_USERS[0]._id) {
      return { exec: jest.fn().mockResolvedValue(MOCK_USERS[0]) };
    } else {
      return { exec: jest.fn().mockResolvedValue(null) };
    }
  };
};

export class MockFactoryFail {
  static findById() {
    return { exec: jest.fn().mockRejectedValue(null) };
  };

  static find() {
    return { exec: jest.fn().mockRejectedValue(null) };
  };

  static findByIdAndUpdate() {
    return { exec: jest.fn().mockRejectedValue(null) };
  };

  static findByIdAndDelete() {
    return { exec: jest.fn().mockRejectedValue(null) };
  };
}