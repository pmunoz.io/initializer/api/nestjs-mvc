export class UserDto {
  readonly name: string;
  password: string;
  readonly email: string;
  active: boolean;
}