import { FORM_VALID_NAME, FORM_UNIQUE_EMAIL } from '../../constant/user.constant';
import * as mongoose from 'mongoose';
import { GenericResponses } from '../../../constant/generic-response.constant';
import {
  EMAIL_REGEX,
  USER_REGEX
} from '../../constant/user.constant';

export const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, GenericResponses.FORM_VALIDATION.REQUIRED],
    match: [USER_REGEX, FORM_VALID_NAME]
  },
  password: {
    type: String,
    required: [true, GenericResponses.FORM_VALIDATION.REQUIRED],
    minlength: 8
  },
  email: {
    type: String,
    required: [true, GenericResponses.FORM_VALIDATION.REQUIRED],
    trim: true,
    lowercase: true,
    unique: [true, FORM_UNIQUE_EMAIL],
    match: [EMAIL_REGEX, GenericResponses.FORM_VALIDATION.EMAIL_FORMAT]
  },
  active: {
    type: Boolean,
    required: [true, GenericResponses.FORM_VALIDATION.REQUIRED]
  }
});