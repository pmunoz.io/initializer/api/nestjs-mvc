
export const FORM_VALID_NAME = 'Please fill a valid name';
export const FORM_UNIQUE_EMAIL = 'The email has already been taken';
export const USER_REMOVE_KEYS = ['__v', 'password'];
export const USER_REGEX = /^[a-zA-Zá-úÁ-Úä-üÄ-Üñ ]*$/;
export const EMAIL_REGEX = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;