import { UserDto } from '../model/dto/user.dto';
import {
  Controller,
  Body,
  Get,
  Param,
  Put,
  Delete,
  UseGuards
} from '@nestjs/common';

import { JwtAuthGuard } from '../../auth/guard/jwt-auth.guard';
import { ResponseObject } from '../../shared/model/response-object.interfaces';
import { User } from '../../user/model/interface/user.interface';
import { UserService } from '../service/user.service';

@UseGuards(JwtAuthGuard)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Get()
  async findAll(): Promise<ResponseObject<User[]>> {
    return await this.userService.findAll();
  }

  @Get('/:userId')
  async findOne(@Param('userId') userId: string): Promise<ResponseObject<User>> {
    return await this.userService.findOne(userId);
  }

  @Put('/:userId')
  async update(@Param('userId') userId: string, @Body() userDto: UserDto): Promise<ResponseObject<User>> {
    return await this.userService.update(userId, userDto);
  }

  @Delete('/:userId')
  async delete(@Param('userId') userId: string): Promise<ResponseObject<User>> {
    return await this.userService.delete(userId);
  }
}