import { Test, TestingModule } from '@nestjs/testing';

import { UserController } from './user.controller';
import { UserService } from '../service/user.service';
import {
  RESPONSE_USERS,
  SERVICE_MOCK,
  MOCK_USERS,
  RESPONSE_USER
} from '../model/mock/user.mock';
import * as _ from 'underscore';

describe("UserController", () => {
  let userController: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [{
        provide: UserService,
        useValue: SERVICE_MOCK
      }]
    }).compile();
    userController = module.get<UserController>(UserController);
  });

  test('sould be defined', () => {
    expect(userController).toBeDefined();
  });

  test('should return all users', async () => {
    const response = await userController.findAll();
    expect(response).toEqual(RESPONSE_USERS);
    expect(SERVICE_MOCK.findAll).toHaveBeenCalled();
  });

  test('should return one user by id', async () => {
    const id = MOCK_USERS[0]._id;
    const response = await userController.findOne(id);

    expect(response).toEqual(RESPONSE_USER);
    expect(SERVICE_MOCK.findOne).toHaveBeenCalled();
    expect(SERVICE_MOCK.findOne).toHaveBeenCalledWith(id);
  });

  test('should return the user updated', async () => {
    const id = MOCK_USERS[0]._id;
    const userToUpdate = _.omit(MOCK_USERS[0], ['_id']);

    const response = await userController.update(id, userToUpdate);

    expect(response).toEqual(RESPONSE_USER);
    expect(SERVICE_MOCK.update).toHaveBeenCalled();
    expect(SERVICE_MOCK.update).toHaveBeenCalledWith(id, userToUpdate);
  });

  test('should return the user deleted', async () => {
    const id = MOCK_USERS[0]._id;

    const response = await userController.delete(id);

    expect(response).toEqual(RESPONSE_USER);
    expect(SERVICE_MOCK.delete).toHaveBeenCalled();
    expect(SERVICE_MOCK.delete).toHaveBeenCalledWith(id);
  });
});