import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { urlencoded, json } from 'body-parser';
import { env } from './config/env';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Basic CORS
  app.use(urlencoded({ extended: true }));
  app.use(json());
  app.enableCors();

  await app.listen(env.port);

}
bootstrap();