# NestJS MVC

NestJS MVC backend API

## Requirements

to run this application you need:

  * [NodeJS](https://nodejs.org/) version 13.8.0 or higher installed
  * an instance of the [MongoDB](https://docs.mongodb.com/manual/) database running

_**Note:** you can reference [env.ts](./src/config/env.ts) file for Environmental Variables (the ALL\_CAPS ones) you need to set up_

--------

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
